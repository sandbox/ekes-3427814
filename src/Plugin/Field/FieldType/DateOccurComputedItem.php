<?php

namespace Drupal\date_occur_computed\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'date_occur_computed' field type.
 *
 * @FieldType(
 *   id = "date_occur_computed",
 *   label = @Translation("Recurring Date Occurrences"),
 *   category = @Translation("General"),
 *   default_formatter = "daterange_plain",
 *   no_ui = TRUE
 * )
 */
class DateOccurComputedItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Start date value'))
      ->setDescription(t('The start of this occurrence.'))
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setSetting('date component', 'value');

    $properties['start_date'] = DataDefinition::create('any')
      ->setLabel(t('Computed start date'))
      ->setDescription(t('The occurrence start DateTime object.'))
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setSetting('date component', 'start_date');

    $properties['end_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('End date value'))
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setSetting('date component', 'end_value');

    $properties['end_date'] = DataDefinition::create('any')
      ->setLabel(t('Computed end date'))
      ->setDescription(t('The computed end DateTime object.'))
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setSetting('date component', 'end_date');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    return [];
  }

}
