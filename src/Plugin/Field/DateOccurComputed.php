<?php

namespace Drupal\date_occur_computed\Plugin\Field;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\date_recur\DateRange;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurFieldItemList;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * A computed field item list.
 */
class DateOccurComputed extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Limit total number of occurrences to return in a computation.
   */
  public const OCCURRENCES_LIMIT = 1000;

  /**
   * Maximum number of occurrences to generate.
   *
   * @var int|null
   */
  protected ?int $occurrencesLimit = NULL;

  /**
   * The date recur field that this comuted field shows occurrences for.
   *
   * @return \Drupal\date_recur\Plugin\Field\FieldType\DateRecurFieldItemList
   *   Recurring date field.
   */
  protected function getRecurField(): DateRecurFieldItemList {
    return $this->getEntity()->get($this->getSetting('original field'));
  }

  /**
   * Maximum number of occurrences to compute.
   *
   * @return int
   *   Maximum number of occurrences to generate.
   */
  public function getOccurrencesLimit(): int {
    if (!is_null($this->occurrencesLimit)) {
      return $this->occurrencesLimit;
    }

    // @todo replace with setting.
    return 1000;
  }

  /**
   * Set maximum number of occurences to compute.
   *
   * @param int $limit
   *   Number to limit to.
   */
  public function setOccurrencesLimit(int $limit) {
    $this->occurrencesLimit = $limit;
  }

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $recur_field = $this->getRecurField();
    $limit = $this->getOccurrencesLimit();

    $delta = 0;
    foreach ($recur_field as $field_item) {
      $helper = $field_item->getHelper();

      $occurrences = $helper->getOccurrences(limit: $limit);

      foreach ($occurrences as $occurrence) {
        if ($occurrence->getStart() && $occurrence->getEnd()) {
          $this->list[$delta] = $this->createItem($delta, $occurrence);
          $delta++;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function createItem($offset = 0, $value = NULL) {
    // Compute values for item if passed the date_recur DateRange.
    if ($value instanceof DateRange) {
      $storage_format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
      $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);
      $start = $value->getStart();
      $end = $value->getEnd();

      // @todo can we settle on our typeddata.
      return parent::createItem($offset, [
        'timezone' => $start->getTimezone()->getName(),
        'start_date' => DrupalDateTime::createFromDateTime($start),
        'end_date' => DrupalDateTime::createFromDateTime($end),
        'value' => $start->setTimezone($storage_timezone)->format($storage_format),
        'end_value' => $end->setTimezone($storage_timezone)->format($storage_format),
      ]);
    }

    return parent::createItem($offset, $value);
  }

  /**
   * Overrridden functions that do not make sense for computed read only.
   */

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
  }

  /**
   * {@inheritdoc}
   */
  public function set($index, $value) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function appendItem($value = NULL) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeItem($index) {
    return $this;
  }

}
