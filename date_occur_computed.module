<?php

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\date_occur_computed\Field\BundleFieldDefinition;
use Drupal\date_occur_computed\Plugin\Field\DateOccurComputed;

/**
 * @file
 * Primary module hooks for Recurring Dates Occurrences Computed Field module.
 */

/**
 * Implements hook_FORM_ID_alter().
 */
function date_occur_computed_form_field_config_edit_form_alter(&$form, FormStateInterface $form_state) {
  // Add option to enable the computed field on the date_recur field settings.
  if (($config = $form_state->getFormObject()->getEntity()) && ($config->getType() == 'date_recur')) {
    $form['third_party_settings']['date_occur_computed']['occurrences_field'] = [
      '#type' => 'checkbox',
      '#title' => \t('Enable occurrences computed field'),
      '#default_value' => $config->getThirdPartySetting('date_occur_computed', 'occurrences_field'),
      '#return_value' => date_occur_computed_field_name($config->getName()),
    ];
  }
}

/**
 * Get occurrence field name.
 */
function date_occur_computed_field_name($date_recur_field) {
  return $date_recur_field . '_occur';
}

/**
 * Implements hook_entity_bundle_field_info_alter().
 */
function date_occur_computed_entity_bundle_field_info_alter(&$fields, EntityTypeInterface $entity_type, $bundle) {
  // Add our computed field for entity bundle date_recur field that has configuration
  // enabled.
  foreach ($fields as $field) {
    if ($field instanceof FieldDefinitionInterface &&
      $field instanceof ConfigEntityInterface &&
      $field->getType() == 'date_recur' &&
      ($field_name = $field->getThirdPartySetting('date_occur_computed', 'occurrences_field'))
    ) {
      $fields[$field_name] = BundleFieldDefinition::create('date_occur_computed')
        ->setName($field_name)
        ->setTargetEntityTypeId($entity_type->id())
        ->setTargetBundle($bundle)
        ->setLabel(new TranslatableMarkup('Recurring date occurrences'))
        ->setReadOnly(TRUE)
        ->setComputed(TRUE)
        ->setTranslatable(FALSE)
        ->setSetting('original field', $field->getName())
        ->setClass(DateOccurComputed::class)
        ->setDisplayConfigurable('view', TRUE);
    }
  }

}

/**
 * Implements hook_field_formatter_info_alter().
 */
function date_occur_computed_field_formatter_info_alter(array &$info) {
  // For now just allow date range plain formatter to be used to display.
  $info['daterange_plain']['field_types'][] = 'date_occur_computed';
}
