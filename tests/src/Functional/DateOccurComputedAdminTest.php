<?php

namespace Drupal\Tests\date_occur_computed\Functional;

use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the administrative UI.
 *
 * @group date_occur_computed
 */
class DateOccurComputedAdminTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'date_occur_computed',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The name of the content type created for testing purposes.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();

    // Create test user.
    $admin_user = $this->drupalCreateUser([
      'administer entity_test content',
      'administer entity_test fields',
      'administer entity_test display',
    ]);
    $this->drupalLogin($admin_user);
  }

  public function testStorage() {
    $occur_field = FieldConfig::loadByName('entity_test', 'entity_test', 'date_recur_field_occur');
    $this->assertEmpty($occur_field);
    $this->drupalGet('/entity_test/structure/entity_test/fields/entity_test.entity_test.date_recur_field');
    $page = $this->getSession()->getPage();
    $page->checkField('third_party_settings[date_occur_computed][occurrences_field]');
    $this->submitForm([], 'Save settings');

    $this->drupalGet('/entity_test/structure/entity_test/display');
    $this->assertSession()->pageTextContains('Recurring date occurrences');

    $this->drupalGet('/entity_test/structure/entity_test/fields/entity_test.entity_test.date_recur_field');
    $page = $this->getSession()->getPage();
    $page->uncheckField('third_party_settings[date_occur_computed][occurrences_field]');
    $this->submitForm([], 'Save settings');

    $this->drupalGet('/entity_test/structure/entity_test/display');
    $this->assertSession()->pageTextNotContains('Recurring date occurrences');
  }

}
