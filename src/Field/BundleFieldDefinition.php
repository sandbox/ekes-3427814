<?php

namespace Drupal\date_occur_computed\Field;

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * A class for defining entity bundle fields.
 *
 * @todo Remove this When https://www.drupal.org/node/2346347 is fixed.
 */
class BundleFieldDefinition extends BaseFieldDefinition {

  /**
   * {@inheritdoc}
   */
  public function isBaseField() {
    return FALSE;
  }

}
