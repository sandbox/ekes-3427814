<?php

namespace Drupal\Tests\date_occur_computed\Kernel;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\date_occur_computed\Plugin\Field\DateOccurComputed;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests computed field values based on Recurring Date occurrences.
 *
 * Somewhat more rigourous testing of the iteration. Trying to wire the iterator
 * into the field more than just generating an array, and then turning it into
 * an iterator for the IteratorAggregate is ... challenging.
 *
 * @group date_occur_computed
 */
class DateOccurComputedValuesTest extends KernelTestBase {

  /**
   * Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|null
   */
  protected ?EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'date_occur_computed',
    'entity_test',
    'datetime',
    'datetime_range',
    'date_recur',
    'field',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');
    $this->installConfig('date_recur');
    $this->installConfig('date_occur_computed');
    $this->entityFieldManager = \Drupal::service('entity_field.manager');

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->setThirdPartySetting('date_occur_computed', 'occurrences_field', 'date_recur_field_occur');
    $this->fieldConfig->save();
  }

  /**
   * Test date_recur_occur_test _entity_base_field_alter third party setting.
   */
  public function testBaseFieldThirdPartySetting() {
    $definitions = $this->entityFieldManager->getFieldDefinitions('entity_test', 'entity_test');
    // Just check the date_recur_occur_test_entity_base_field_info_alter().
    $recurDefinition = $definitions['date_recur_field'];
    $this->assertEquals('date_recur_field_occur', $recurDefinition->getThirdPartySetting('date_occur_computed', 'occurrences_field'));

    // Check adding third party setting to base field added computed field.
    $occurDefinition = $definitions['date_recur_field_occur'];
    $this->assertNotNull($occurDefinition);
  }

  /**
   * Basic occurrences test.
   */
  public function testOccurences() {
    $entity = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $entity->date_recur_field = [
      'value' => '2014-06-15T23:00:00',
      'end_value' => '2014-06-16T07:00:00',
      'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR',
      'infinite' => '1',
      'timezone' => 'Australia/Darwin',
    ];
    // Validation tests getIterator() looping over the field values.
    $entity->validate();
    $entity->save();

    $this->assertEquals($entity->date_recur_field->value, $entity->date_recur_field_occur->get(0)->value);
    $this->assertEquals($entity->date_recur_field->end_value, $entity->date_recur_field_occur->get(0)->end_value);
    $this->assertEquals($entity->date_recur_field->start_date, $entity->date_recur_field_occur->get(0)->start_date);
    $this->assertEquals($entity->date_recur_field->end_date, $entity->date_recur_field_occur->get(0)->end_date);
    $this->assertEquals($entity->date_recur_field->timezone, $entity->date_recur_field_occur->get(0)->timezone);

    $this->assertEquals('2014-06-15T23:00:00', $entity->date_recur_field_occur->get(0)->value);
    $this->assertEquals('2014-06-16T07:00:00', $entity->date_recur_field_occur->get(0)->end_value);
    $this->assertEquals('2014-06-29T23:00:00', $entity->date_recur_field_occur->get(10)->value);
    $this->assertEquals('2014-06-30T07:00:00', $entity->date_recur_field_occur->get(10)->end_value);

    $this->assertGreaterThan(0, $entity->date_recur_field_occur->count());
    $this->assertEquals(DateOccurComputed::OCCURRENCES_LIMIT, $entity->date_recur_field_occur->count());
  }

  /**
   * Test count.
   */
  public function testCount() {
    $entity = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $entity->date_recur_field[] = [
      'value' => '2014-06-15T23:00:00',
      'end_value' => '2014-06-16T07:00:00',
      'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;COUNT=5',
      'infinite' => '0',
      'timezone' => 'Australia/Darwin',
    ];
    $entity->save();
    $this->assertEquals(5, $entity->date_recur_field_occur->count());

    $entity = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $entity->date_recur_field[] = [
      'value' => '2014-06-15T23:00:00',
      'end_value' => '2014-06-16T07:00:00',
      'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;COUNT=5',
      'infinite' => '0',
      'timezone' => 'Australia/Darwin',
    ];
    $entity->date_recur_field[] = [
      'value' => '2015-06-15T23:00:00',
      'end_value' => '2015-06-16T07:00:00',
      'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;COUNT=10',
      'infinite' => '0',
      'timezone' => 'Australia/Darwin',
    ];
    $entity->save();
    $this->assertEquals(15, $entity->date_recur_field_occur->count());
  }

  /**
   * Test Empty.
   */
  public function testIsEmpty() {
    $entity = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $entity->save();
    $this->assertTrue($entity->date_recur_field_occur->isEmpty());
    $this->assertEquals(0, $entity->date_recur_field_occur->count());
    $this->assertEmpty($entity->date_recur_field_occur->getValue());

    $entity = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $entity->date_recur_field[] = [
      'value' => '2014-06-15T23:00:00',
      'end_value' => '2014-06-16T07:00:00',
      'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;COUNT=5',
      'infinite' => '0',
      'timezone' => 'Australia/Darwin',
    ];
    $entity->save();
    $this->assertFalse($entity->date_recur_field_occur->isEmpty());
  }

  /**
   * Test offset exists.
   */
  public function testOffsetExists() {
    $entity = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $entity->date_recur_field[] = [
      'value' => '2014-06-15T23:00:00',
      'end_value' => '2014-06-16T07:00:00',
      'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;COUNT=5',
      'infinite' => '0',
      'timezone' => 'Australia/Darwin',
    ];
    $entity->save();
    $this->assertTrue($entity->date_recur_field_occur->offsetExists(0));
    $this->assertTrue($entity->date_recur_field_occur->offsetExists(4));
    $this->assertFalse($entity->date_recur_field_occur->offsetExists(5));
  }

  /**
   * Tests storage timezone is returned.
   *
   * Not the timezone used for current request, or default to UTC per storage.
   */
  public function testOccurrencesTimezone() {
    // Set the timezone to something different than UTC or storage.
    date_default_timezone_set('Pacific/Wake');

    $tzChristmas = new \DateTimeZone('Indian/Christmas');
    $entity = EntityTest::create();
    $entity->date_recur_field = [
      'value' => '2014-06-15T23:00:00',
      'end_value' => '2014-06-16T07:00:00',
      'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR',
      'infinite' => '1',
      'timezone' => $tzChristmas->getName(),
    ];

    $occurrence_start = $entity->date_recur_field_occur->get(0)->start_date;
    $occurrence_end = $entity->date_recur_field_occur->get(0)->end_date;

    // Christmas island is UTC+7, so start time will be 6am.
    $assertDateStart = new DrupalDateTime('6am 2014-06-16', $tzChristmas);
    $assertDateEnd = new DrupalDateTime('2pm 2014-06-16', $tzChristmas);

    $this->assertTrue($assertDateStart == $occurrence_start);
    $this->assertEquals($tzChristmas->getName(), $occurrence_start->getTimezone()->getName());
    $this->assertTrue($assertDateEnd == $occurrence_end);
    $this->assertEquals($tzChristmas->getName(), $occurrence_end->getTimezone()->getName());
  }

}
